using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class FireballClickSpawner : MonoBehaviour
{
    private float fireballcooldown;
    [SerializeField]
    private float fireballspawntime;
    [SerializeField]
    private GameObject fireball;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (fireballcooldown <= 0)
        {
            ClickSpawner();
        }
        else
        {
            fireballcooldown -= Time.deltaTime;
        }
        
        
            
    }


    public void ClickSpawner()
    {
        if (Input.GetMouseButton(0)){
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 fireball_position = new Vector3(mousePos.x, this.transform.position.y, this.transform.position.z);
            Instantiate(fireball, fireball_position, new Quaternion(0f, 0f, 0f, 0f));
            fireballcooldown = fireballspawntime;

        }
        else if (Input.GetMouseButton(1))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 fireball_position = new Vector3(mousePos.x, this.transform.position.y, this.transform.position.z);
            Instantiate(fireball, fireball_position, new Quaternion(0f, 0f, 0f, 0f));
            fireballcooldown = fireballspawntime;
        }
        else if (Input.GetMouseButton(2))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 fireball_position = new Vector3(mousePos.x, this.transform.position.y, this.transform.position.z);
            Instantiate(fireball, fireball_position, new Quaternion(0f, 0f, 0f, 0f));
            fireballcooldown = fireballspawntime;
        }
            
    }
   
}
