using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireballSpawn : MonoBehaviour
{

    private float fireballcooldown;
    [SerializeField]
    private float fireballspawntime;
    public GameObject _fireball;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
         if (fireballcooldown <= 0)
        {
            FireballSpawner();
        }
        else
        {
            fireballcooldown -= Time.deltaTime;
        }
    }
    public void FireballSpawner()
    {
        float randomX = Random.Range(-18f, 12f);

        Vector3 fireball_position = new Vector3(randomX, this.transform.position.y, this.transform.position.z);
        Instantiate(_fireball, fireball_position, new Quaternion(0f, 0f, 0f, 0f));
        fireballcooldown = fireballspawntime;
    }
}
