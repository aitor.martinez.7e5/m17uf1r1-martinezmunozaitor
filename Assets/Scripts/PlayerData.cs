using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerData : MonoBehaviour
{


    private Rigidbody2D rb;
    private SpriteRenderer sr;
    [SerializeField]
    private float fuerzasalto;
    [SerializeField]
    [Range(1, 10)] private float velocidad;
    [SerializeField]
    private float weight;
    private RaycastHit2D raycastHit;
    private bool isJumping;
    public Animator animator;
    private BoxCollider2D box;




    // Start is called before the first frame update
    void Start()
    {

        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        box = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Movimiento();
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Saltar();
        }
        PlatformFalling();


    }

    public void Movimiento()
    {


        float movimientoH = Input.GetAxisRaw("Horizontal");

        rb.velocity = new Vector2(movimientoH * velocidad, rb.velocity.y);


        if (movimientoH != 0 && !isJumping)
        {
            animator.SetBool("IsRunning", true); //Si se est� moviendo, reproduzco la animaci�n
            animator.SetBool("Idle", false);
        }
        else
        {
            animator.SetBool("IsRunning", false); //Si no, la paro
            if (!isJumping) animator.SetBool("Idle", true);

        }


        if (movimientoH > 0)
        {
            sr.flipX = false;
        }
        else if (movimientoH < 0)
        {
            sr.flipX = true;
        }



    }

    public void Saltar()
    {
        if (!isJumping)
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * fuerzasalto, ForceMode2D.Impulse);

        }

    }

    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.collider.CompareTag("Suelo")) isJumping = false;

    }

    private void OnCollisionExit2D(Collision2D other)
    {
        animator.SetBool("IsJumping", true);
        animator.SetBool("Idle", false);

        isJumping = true;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.CompareTag("Suelo")) isJumping = false;
        animator.SetBool("IsJumping", false);
    }

    public void PlatformFalling()
    {


        if (transform.position.y < -9f)
        {
            SceneManager.LoadScene("Menu");
        }
    }
}
