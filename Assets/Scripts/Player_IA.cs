using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_IA : MonoBehaviour
{
    public Animator animator;
  
    private SpriteRenderer sr;
    private Rigidbody2D rb;
    private GameObject _detector;
    private BoxCollider2D _detectorcollider;

    
    [SerializeField]
    [Range(1, 10)] private float velocidad;

    private bool left = true;
    private bool right = false;

    // Start is called before the first frame update
    void Start()
    {
        _detector = GameObject.Find("Detector");
        _detectorcollider = _detector.GetComponent<BoxCollider2D>();
        animator.SetBool("IsRunning", true);
        animator.SetBool("Idle", false);
        sr = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        

    }

    // Update is called once per frame
    void Update()
    {
        Movimiento();
    }

    public void Movimiento()
    {
         if (right == false && left == true)
        {
          transform.Translate(Vector3.left * velocidad * Time.deltaTime);
          sr.flipX = true;
          _detectorcollider.transform.rotation = new Quaternion(0,180,0,0) ;
        }else if (left==false&&right==true)
        {
           transform.Translate(Vector3.right * velocidad * Time.deltaTime);
            _detectorcollider.transform.rotation = new Quaternion(0, 0, 0, 0);
            sr.flipX = false;
        }


        if (transform.position.x <= -15f&&left == true && right == false)
        {
           left = false;
            right = true;
        }

        if (transform.position.x >= 10f && left == false && right == true)
        {

            left = true;
            right = false;
        }
        
       
        
      
   


    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log(collider);
        if (collider.CompareTag("Fireball")&&right==true&&left==false)
        {
            right=false;
            left = true;

        }else if (collider.CompareTag("Fireball") && right == false && left == true)
        {
            right = true;
            left = false;

        }


    }

}
