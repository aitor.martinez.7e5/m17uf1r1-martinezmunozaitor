using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;


public class SceneChanger: MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void ChangeToGame1()
    {

        SceneManager.LoadScene("Game1");
    }

    public void ChangeToGame2()
    {

       SceneManager.LoadScene("Game2");
    }

    
}


