using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private double seconds;
    [SerializeField]
    private Text contador;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        seconds -= Time.deltaTime;
        contador.GetComponent<Text>().text ="00:"+Mathf.Round((float)seconds).ToString();
        if (seconds < 0) SceneManager.LoadScene("Game1");        
    }
}
